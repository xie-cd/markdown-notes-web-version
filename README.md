# markdown笔记网页版

#### Description
通过网页显示咱们写的笔记


#### 开始拥有自己的笔记

- 在app.js文件中修改端口
```js
app.listen(3000)//修改
console.log('http://localhost:3000');
```

- 在router文件夹下的router.js文件中，找到`/api/login`路由，设置一个你的登入密码：

```js
router.post('/api/login',async ctx =>{
    let pwd = ctx.request.body.pwd;

    if(pwd==='4433'){//将4433修改为自己的登入密码
        ctx.render('user.html',{})
    }
    else{
        ctx.body={err:-200,msg:'登入失败'}
    }
})
```
#### 小提示

![图片被吃了](/img/xq.png)


#### 效果展示
![图片被吃了](/img/home.png)
![图片被吃了](/img/note.png)
![图片被吃了](/img/upload.png)

#### 上传服务器
切记，我使用的是npm，不是yarn

然后只要复制myApp中的文件到服务器中就可以了

