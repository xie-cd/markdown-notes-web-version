const nunjucks=require('nunjucks');
const path = require('path')

let env=nunjucks.configure(path.resolve(__dirname,'../views'),{
    autoescape:true,
    watch:true,
    noCache: true
})


module.exports = async (ctx,next)=>{
    ctx.render = (name,data)=>{
        ctx.body = env.render(name,data)
    }
   await next();
}