Date.prototype.format = function (format = 'yyyy-MM-dd HH:mm') {
    let dateTime = {
        yyyy: this.getFullYear(),
        MM: this.getMonth() + 1,
        dd: this.getDate() < 10 ? "0" + this.getDate() : this.getDate(),
        HH: this.getHours() < 10 ? "0" + this.getHours() : this.getHours(),
        mm: this.getMinutes() < 10 ? "0" + this.getMinutes() : this.getMinutes()
    }
    var reg = /^(yyyy|YYYY)(\-MM\-|\/MM\/)(dd|DD) (HH|hh)(:mm)$/
    if (reg.test(format)) {
        for (const key in dateTime) {
            format = format.replace(key, dateTime[key]);

        }
        return format;
    }

    return Date();
}

Date.prototype.get = function () {
    let dateTime = {
        yyyy: this.getFullYear(),
        MM: this.getMonth() + 1,
        dd: this.getDate() < 10 ? "0" + this.getDate() : this.getDate(),
        HH: this.getHours() < 10 ? "0" + this.getHours() : this.getHours(),
        mm: this.getMinutes() < 10 ? "0" + this.getMinutes() : this.getMinutes(),
        ms: this.getSeconds()
    }
    let obj = {
        date: `${dateTime.yyyy}-${dateTime.MM}-${dateTime.dd}`,
        time: `${dateTime.HH}:${dateTime.mm}:${dateTime.ms}`,
        week: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'].filter((item, index) => index === this.getDay()).toString()
    }

    return obj;
}